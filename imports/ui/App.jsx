import React, { Component, PropTypes } from 'react';
import { createContainer } from 'meteor/react-meteor-data';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

import SimpleTable from './Table';

// database - collection
import { Person } from '../api/person';

class App extends Component {

  render() {
    if(this.props.loaded && this.props.people) {
      return (
        <MuiThemeProvider>
          <div className="container">
            <div className="row">
              <SimpleTable len={this.props.people.length} people={this.props.people.slice(0, 20)}/>
            </div>
          </div>
        </MuiThemeProvider>
      )
    }
    return (
      <div className="container">
        <h5>Loading...</h5>
      </div>
    )
  }
}

App.propTypes = {
  people: PropTypes.array.isRequired
}

export default createContainer(() => {

  const handle = Meteor.subscribe("person");

  return {
    loaded: handle.ready(),
    people: Person.find({}).fetch()
  }
}, App);
