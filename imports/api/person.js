import { Mongo } from 'meteor/mongo';

export const Person = new Mongo.Collection('person');

const PersonSchema = new SimpleSchema({
  name: {type: String },
  gender: {type: String },
  description: {type: String },
  age: {type: Number },
  date_of_birth: {type: String },
  nation: {type: String },
  relationship_status: {type: String },
});

Person.attachSchema(PersonSchema);
