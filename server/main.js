import { Meteor } from 'meteor/meteor';

import { Person } from '../imports/api/person.js';

function randomDate(start, end) {
    return new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
}

Meteor.startup(() => {
  // code to run on server at startup
  if(Person.find({}).count() === 0) {
    const descriptions = Assets.getText("descriptions.txt").split("\n\n");
    const names = Assets.getText("first_names.txt").split("\n");
    const genders = ["male", "female", "gay", "lesbian", "unknow"];
    const nations = Assets.getText("nations.txt").split("\n");
    const relationships = ["single", "in a relationship", "engaged",
                           "married", "it's complicated", "in an open relationship", "separated",
                           "divorced", "in a civil union", "in a domestic partnership"];
    for(let i = 0; i < 100000; i++) {
      const name = names[Math.floor(Math.random() * (names.length - 1))];
      const description = descriptions[Math.floor(Math.random() * (descriptions.length - 1))];
      const gender = genders[Math.floor(Math.random() * (genders.length - 1))];
      const age = 1 + Math.floor(Math.random() * 120);
      const date_of_birth = randomDate(new Date(1900, 0, 1), new Date());
      const nation = nations[Math.floor(Math.random() * (nations.length - 1))];
      const relationship_status = relationships[Math.floor(Math.random() * (relationships.length - 1))];
      Person.insert({
        name,
        gender,
        description,
        age,
        date_of_birth,
        nation,
        relationship_status,
      });
    }
    console.log("\n~~~>Total Records: " + Person.find().count() + "<~~~\n");
  }

  Meteor.publish('person', function() {
    return Person.find({});
  });

});
